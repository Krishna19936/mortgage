package com.example.mortgage.config;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class CheckDate {
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	public long checkthedata(String dt,String d) {
		
		if(dt.matches("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$") 
				)
		{
			String[] dt1 = dt.split("-");
		
		LocalDate start = LocalDate.of(Integer.parseInt(dt1[2]), Integer.parseInt(dt1[1]), Integer.parseInt(dt1[0]));
		LocalDate end = LocalDate.now(); 
		if(d.equalsIgnoreCase("m")) {
			return ChronoUnit.MONTHS.between(start, end);
		}
		if(d.equalsIgnoreCase("y")) {
			return ChronoUnit.YEARS.between(start, end);
		}
		
		}
		 return 0;
		
	}
	
	public long generaterandnumber(int n) {
		
		Random rand = new Random(); 
		 if(n == 2) {
		        return 10 + rand.nextInt(90);
		         }
		
         if(n == 6) {
        return 100000 + rand.nextInt(900000);
         }
         return 100000 + rand.nextInt(900000);
         
	}
	
	public String generatePassword() {
		
		 int leftLimit = 48; 
 	    int rightLimit = 122; 
 	    int targetStringLength = 10;
 	    Random random = new Random();
   String generatedString = random.ints(leftLimit, rightLimit + 1)
 	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
 	      .limit(targetStringLength)
 	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
 	      .toString();

 	    return generatedString;
	}
	
	
	

}
