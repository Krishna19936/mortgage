package com.example.mortgage.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

	
	

public class MailSender {
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	
		public void sendEmail(String email,String password, String loginUserId) {

	        SimpleMailMessage msg = new SimpleMailMessage();
	        msg.setTo(email);

	        msg.setSubject("Please login with these credentials");
	        msg.setText("UserId:" + loginUserId + ", \n" + 
	        		"password:" + password);
	        

	        javaMailSender.send(msg);

	    }

}
