package com.example.mortgage.controller;



import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.mortgage.dto.AccountSummaryDTO;
import com.example.mortgage.dto.UserDTO;
import com.example.mortgage.dto.UserLoginDTO;
import com.example.mortgage.dto.UserLoginReqDTO;
import com.example.mortgage.dto.UserRegResponseDTO;
import com.example.mortgage.entity.User;
import com.example.mortgage.exception.InvalidMortgageRequest;
import com.example.mortgage.exception.NoRecordsFoundException;
import com.example.mortgage.exception.ResponseStatus;
import com.example.mortgage.service.AccountService;
import com.example.mortgage.service.UserService;


import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MortgageController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	AccountService accountService;

	
	@ApiOperation(value = "Register a user")
	@PostMapping("/users")
	public ResponseEntity<UserRegResponseDTO> registerUser(@Valid @RequestBody UserDTO userDTO) throws InvalidMortgageRequest{
			 
	
		return new ResponseEntity<>(userService.userRegistration(userDTO), HttpStatus.OK);

	}
	
	/**
     * Customer login
     * 
     * @param customerid
     * @param password
     * @return message
     * @throws EmailorPasswordException
     */
    @ApiOperation("Login web application")
    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity userlogin(@RequestBody UserLoginReqDTO userreqDTO) {

 

        User userlogon = userService.userlogin(userreqDTO);

 

        if (userlogon == null) {
            ResponseStatus error = new ResponseStatus();
            error.setMessage("Incorrect customerID or Passowrd");
            error.setStatuscode(400);
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);

 

        } else {
            
        	UserLoginDTO u = new UserLoginDTO();
            u.setUserId(userlogon.getUserId());
            
            u.setFullUserName(userlogon.getFirstName().concat(userlogon.getSurName()));
        
            return ResponseEntity.status(HttpStatus.CREATED).body(u);
        }

 

    }
    
    
    @ApiOperation("fetch user account summary details")
    @GetMapping("/users/{userId}/accounts")
	public ResponseEntity<List<AccountSummaryDTO>> userAccountSummary(@PathVariable("userId") long userId) throws NoRecordsFoundException  {
		
		    return new ResponseEntity<>(accountService.fetchuseraccdetails(userId), HttpStatus.OK);
		
	}
	
}


