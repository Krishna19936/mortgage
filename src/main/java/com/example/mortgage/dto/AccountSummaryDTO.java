package com.example.mortgage.dto;


import java.util.List;

import com.example.mortgage.entity.Statement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountSummaryDTO {

	
		
		private long balance;

		private String accountType;

		private long accountNumber;
		private List<Statement> st;

}
