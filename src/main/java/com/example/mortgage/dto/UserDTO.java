package com.example.mortgage.dto;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class UserDTO {
	
	@Email(message="Invalid email id, try again")
	@NotNull(message= "User Email Id must not be empty")
	private String email;
    
	@NotNull(message= "employmentStatus cannot be empty")
	private String employmentStatus;

	@NotNull(message= "occupation  cannot be empty")
	private String occupation;
	
	@NotNull(message= "contractTpye  cannot be empty")
	private String contractTpye;
	
	@NotNull(message= "jobStartDate  cannot be empty")
	private String jobStartDate;
	
	@NotNull(message= "title cannot be empty")
	private String title;
	
	@NotNull(message= "firstName cannot be empty")
	private String firstName;
	
	
	private String middleName;
	
	@NotNull(message= "surName cannot be empty")
	private String surName;
	
	@NotNull(message= "dateOfBirth cannot be empty")
	private String dateOfBirth;
	
	@NotNull(message= "phoneNumber cannot be empty")
	private String phoneNumber;
	
	@NotNull(message= "operationType cannot be empty")
	private String operationType;

	@NotNull(message= "peopleApplying cannot be empty")
	private String peopleApplying;

	@NotNull(message= "propertyCost cannot be empty")
	private long propertyCost;
	
	@NotNull(message= "deposit cannot be empty")
	private long	deposit;
		
	

}
