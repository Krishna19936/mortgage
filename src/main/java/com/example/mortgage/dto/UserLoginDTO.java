package com.example.mortgage.dto;

public class UserLoginDTO {
	
	private Long userId;
	   
	
    private String fullUserName;
   
   
    public String getFullUserName() {
        return fullUserName;
    }
    public void setFullUserName(String fullUserName) {
        this.fullUserName = fullUserName;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
