package com.example.mortgage.dto;

public class UserLoginReqDTO {
	
private String loginUserId;
	   
	
    private String password;


	public String getLoginUserId() {
		return loginUserId;
	}


	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}




}
