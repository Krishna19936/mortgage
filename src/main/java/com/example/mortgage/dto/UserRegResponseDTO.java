package com.example.mortgage.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRegResponseDTO {
	
	private String LoginUserId;

	private String password;

	private long mortgageAccountNumber;
	private long transactionalAccountNumber;
	

}
