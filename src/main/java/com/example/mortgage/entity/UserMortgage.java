package com.example.mortgage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "usermortgage")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserMortgage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "usermortgageId")
	private Long usermortgageId;

	
	private String operationType;

	private String peopleApplying;

	private long propertyCost;
	
	private long	deposit;

}
