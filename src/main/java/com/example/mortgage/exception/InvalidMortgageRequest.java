package com.example.mortgage.exception;

public class InvalidMortgageRequest extends Exception {

	
	private static final long serialVersionUID = 1L;

	public InvalidMortgageRequest(String message) {
		super(message);
	}

}

