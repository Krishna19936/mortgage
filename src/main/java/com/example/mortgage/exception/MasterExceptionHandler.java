package com.example.mortgage.exception;

import java.util.ArrayList;
import java.util.List;



import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;




@ControllerAdvice
public class MasterExceptionHandler extends ResponseEntityExceptionHandler {


	@ExceptionHandler(InvalidMortgageRequest.class)
	public ResponseEntity<ResponseStatus> handleInvalidMortgageRequest(InvalidMortgageRequest details) {
		
		ResponseStatus errorStatus = new ResponseStatus();
	        errorStatus.setMessage(details.getLocalizedMessage());
	        errorStatus.setStatuscode(400);
	        return new ResponseEntity<>(errorStatus, HttpStatus.BAD_REQUEST);


	}
	
	@ExceptionHandler(NoRecordsFoundException.class)
	public ResponseEntity<ResponseStatus> handleNoRecordsFoundException(NoRecordsFoundException details) {
		
		ResponseStatus errorStatus = new ResponseStatus();
	        errorStatus.setMessage(details.getLocalizedMessage());
	        errorStatus.setStatuscode(404);
	        return new ResponseEntity<>(errorStatus, HttpStatus.NOT_FOUND);


	}

}
