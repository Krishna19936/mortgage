package com.example.mortgage.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.mortgage.entity.Account;




public interface AccountRepository extends JpaRepository<Account, Long> {

	@Query(value = "select distinct user_id from account where balance < 0 and account_type = 'Mortgage'", nativeQuery = true)
	List<Long> fetchusers();
	
	
	@Query(value = "select balance from account where user_id = ?1 and account_type = ?2", nativeQuery = true)
	long fetchaccbalance(long uid,String type);
	
	@Query(value = "select * from account where user_id = ?1 and account_type = ?2", nativeQuery = true)
	Optional<Account> findByuserIdAndaccountType(long uid, String type);
	
	@Query(value = "select * from account where user_id = ?1", nativeQuery = true)
	Optional<List<Account>> findByuseraccounts(long uid);
	
	
	
	@Transactional
	@Modifying
	@Query(value="Update account set balance = ?1 where user_id = ?2 and account_type = ?3",nativeQuery=true)
	public int updateaccountbalance(long updatedbal, long uid, String type);

}

