package com.example.mortgage.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.mortgage.entity.Statement;

public interface StatementRepository extends JpaRepository<Statement, Long> {
	
	@Query(value = "select * from statement where account_id = ?1", nativeQuery = true)
	Optional<List<Statement>> findaccountstatements(long aid);

}
