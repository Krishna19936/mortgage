package com.example.mortgage.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.example.mortgage.entity.UserMortgage;



public interface UserMortgageRepository extends JpaRepository<UserMortgage, Long> {

	

}

