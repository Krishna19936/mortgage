package com.example.mortgage.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.mortgage.entity.User;




public interface UserRepository extends JpaRepository<User, Long> {

	

	Optional<User> findByphoneNumber(String pnumber);
	
	@Query(value="select * FROM user WHERE login_user_id = ?1 and password = ?2", nativeQuery = true)
    User findByLoginUserIdAndPassword(String loginUserId, String password);

	

}

