package com.example.mortgage.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.mortgage.dto.AccountSummaryDTO;
import com.example.mortgage.dto.UserDTO;
import com.example.mortgage.exception.InvalidMortgageRequest;
import com.example.mortgage.exception.NoRecordsFoundException;


@Service
public interface AccountService {
	
  public List<Long> generateUserAccounts(UserDTO userDTO) throws InvalidMortgageRequest;
	  
  public int runBatchprocess();
  
  public List<AccountSummaryDTO> fetchuseraccdetails(long userId) throws NoRecordsFoundException;
  

}
