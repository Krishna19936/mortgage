package com.example.mortgage.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.mortgage.config.CheckDate;
import com.example.mortgage.dto.AccountSummaryDTO;
import com.example.mortgage.dto.UserDTO;
import com.example.mortgage.entity.Account;
import com.example.mortgage.entity.Statement;
import com.example.mortgage.entity.User;

import com.example.mortgage.exception.InvalidMortgageRequest;
import com.example.mortgage.exception.NoRecordsFoundException;
import com.example.mortgage.repository.AccountRepository;
import com.example.mortgage.repository.StatementRepository;
import com.example.mortgage.repository.UserMortgageRepository;
import com.example.mortgage.repository.UserRepository;


@Service
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	AccountRepository accountrepository;
	
	@Autowired
	UserMortgageRepository usermortgagerepository;

	@Autowired
	UserRepository userrepository;
	
	@Autowired
	StatementRepository statementrepository;
	
	

	@Override
	public List<Long> generateUserAccounts(UserDTO userDTO) throws InvalidMortgageRequest{
		
		
		Optional<User> optionalUser = userrepository.findByphoneNumber(userDTO.getPhoneNumber());
		if (!optionalUser.isPresent()) {
			throw new  InvalidMortgageRequest("Invalid Mortgage Request");
		}
		
		long depo = userDTO.getDeposit();
		long propcost = userDTO.getPropertyCost();
		long transbalance = propcost - depo;
		long mortbalance = depo - propcost;
		List<Long> res = new ArrayList<>();
		List<Account> aclist = new ArrayList<>();
		
		Account ac = new Account();
		CheckDate cd = new CheckDate();
		
		
		
		long macc = cd.generaterandnumber(6);
		ac.setAccountNumber(macc);
		res.add(macc);
		ac.setAccountType("Mortgage");
		ac.setBalance(mortbalance);
	    aclist.add(ac);
	    
	  
	    
	    Account ac1 = new Account();
	    long tacc = cd.generaterandnumber(6);
		ac1.setAccountNumber(tacc);
		ac1.setBalance(transbalance);
		
		res.add(tacc);
		ac1.setAccountType("Transactional");
		aclist.add(ac1);
		optionalUser.get().setAccounts(aclist);
		 userrepository.save(optionalUser.get());
		 
		 return res;
		
		
		
		
		
	}
	
	@Override
	@Scheduled(initialDelay = 5000, fixedRate = 10000)
	public int runBatchprocess(){
	    
		
		
		List<Long> uids = accountrepository.fetchusers();
		if(uids.isEmpty()) {
			return 0;
		}
		
		for(int i = 0; i<uids.size(); i++) {
			
			
			long mbal = accountrepository.fetchaccbalance(uids.get(i), "Mortgage");
			long tbal = accountrepository.fetchaccbalance(uids.get(i), "Transactional");
			long deductamount = 20000;
			long tbalfinal = tbal - deductamount;
			long mbalfinal = mbal + deductamount;
			
			accountrepository.updateaccountbalance(tbalfinal, uids.get(i), "Transactional");
			accountrepository.updateaccountbalance(mbalfinal, uids.get(i), "Mortgage");
			
			
			
			Optional<Account> optionalAccount = accountrepository.findByuserIdAndaccountType(uids.get(i), "Mortgage");
			if(optionalAccount.isPresent()) {
				
				Statement st = new Statement();
				st.setAmount(deductamount);
				if(mbal >= 0) {
					st.setComment("Your Mortgage is cleared");
				}
				st.setComment("amount credited to Mortgage account");
				st.setAccountId(optionalAccount.get().getAccountId());
				statementrepository.save(st);
				
			   
			}
			
			
			
			Optional<Account> optionalAccount1 = accountrepository.findByuserIdAndaccountType(uids.get(i), "Transactional");
			if(optionalAccount1.isPresent()) {
				Statement st = new Statement();
				st.setAmount(-deductamount);
				if(mbal >= 0) {
					st.setComment("Your Mortgage is cleared");
				}
				st.setComment("amount debited from Transactional account");
				st.setAccountId(optionalAccount1.get().getAccountId());
				statementrepository.save(st);
				
				
			}
			
			
			
			
		}
		
		return 1;
	}

	@Override
	public List<AccountSummaryDTO> fetchuseraccdetails(long userId) throws NoRecordsFoundException{
		
		
		List<AccountSummaryDTO> asdlist = new ArrayList<>();
		Optional<List<Account>> aclist = accountrepository.findByuseraccounts(userId);
		if(!aclist.isPresent()) {
			throw new NoRecordsFoundException("No user accounts found");
		}
		List<Account> aclist2 = aclist.get();
		
		for(int i = 0; i<aclist2.size(); i++) {
			Account ac;
			AccountSummaryDTO asd = new AccountSummaryDTO();
			ac = aclist2.get(i);
			asd.setAccountNumber(ac.getAccountNumber());
			asd.setAccountType(ac.getAccountType());
			asd.setBalance(ac.getBalance());
			Optional<List<Statement>> stlist = statementrepository.findaccountstatements(ac.getAccountId());
			if(!stlist.isPresent()) {
				throw new NoRecordsFoundException("no statements found for this account"); 
			}
			List<Statement> stlis1 = stlist.get();
			asd.setSt(stlis1);
			asdlist.add(asd);
			
			
			
		}
		return asdlist;
		
	}
	
	
	
	
   
	
	
}
