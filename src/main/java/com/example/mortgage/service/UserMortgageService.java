package com.example.mortgage.service;



import org.springframework.stereotype.Service;

import com.example.mortgage.dto.UserDTO;
import com.example.mortgage.exception.InvalidMortgageRequest;
import com.example.mortgage.exception.ResponseStatus;

@Service
public interface UserMortgageService {
	
	 public ResponseStatus addUserMortgages(UserDTO userDTO) throws InvalidMortgageRequest;
	  

}
