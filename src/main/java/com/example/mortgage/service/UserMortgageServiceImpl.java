package com.example.mortgage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.mortgage.dto.UserDTO;

import com.example.mortgage.entity.User;
import com.example.mortgage.entity.UserMortgage;
import com.example.mortgage.exception.InvalidMortgageRequest;
import com.example.mortgage.exception.ResponseStatus;
import com.example.mortgage.repository.UserMortgageRepository;
import com.example.mortgage.repository.UserRepository;

@Service
public class UserMortgageServiceImpl implements UserMortgageService{
	
	@Autowired
	UserMortgageRepository usermortgagerepository;

	@Autowired
	UserRepository userrepository;

	@Override
	public ResponseStatus addUserMortgages(UserDTO userDTO) throws InvalidMortgageRequest {
		
		Optional<User> optionalUser = userrepository.findByphoneNumber(userDTO.getPhoneNumber());
		if(userDTO.getPropertyCost() < 100000 || !optionalUser.isPresent() || userDTO.getDeposit() < 0) {
			throw new  InvalidMortgageRequest("Invalid Mortgage Request");
		}
		
		List<UserMortgage> umlist = new ArrayList<>();
		UserMortgage umort = new UserMortgage();
		umort.setPeopleApplying(userDTO.getPeopleApplying());
		umort.setOperationType(userDTO.getOperationType());
		umort.setDeposit(userDTO.getDeposit());
		umort.setPropertyCost(userDTO.getPropertyCost());
		umlist.add(umort);
		optionalUser.get().setUsermortgages(umlist);
		 userrepository.save(optionalUser.get());
		 ResponseStatus rs = new ResponseStatus();
		 rs.setMessage("Mortgage updated");
		 rs.setStatuscode(201);
		
		return rs;
	}

}
