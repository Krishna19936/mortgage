package com.example.mortgage.service;

import org.springframework.stereotype.Service;

import com.example.mortgage.dto.UserDTO;
import com.example.mortgage.dto.UserLoginReqDTO;
import com.example.mortgage.dto.UserRegResponseDTO;
import com.example.mortgage.entity.User;
import com.example.mortgage.exception.InvalidMortgageRequest;

@Service
public interface UserService {
	
	public UserRegResponseDTO userRegistration(UserDTO userDTO) throws InvalidMortgageRequest;
	
	User userlogin(UserLoginReqDTO userreqDTO);

}
