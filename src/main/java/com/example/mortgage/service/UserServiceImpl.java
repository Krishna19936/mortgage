package com.example.mortgage.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mortgage.config.CheckDate;

import com.example.mortgage.dto.UserDTO;
import com.example.mortgage.dto.UserLoginReqDTO;
import com.example.mortgage.dto.UserRegResponseDTO;

import com.example.mortgage.entity.User;

import com.example.mortgage.exception.InvalidMortgageRequest;
import com.example.mortgage.repository.AccountRepository;
import com.example.mortgage.repository.UserMortgageRepository;
import com.example.mortgage.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	AccountRepository accountrepository;
	
	@Autowired
	UserMortgageRepository usermortgagerepository;

	@Autowired
	UserRepository userrepository;
	
	@Autowired
	UserMortgageService usermortgageservice;
	
	@Autowired
	AccountService accountservice;
	
	
	
	@Override
	public UserRegResponseDTO userRegistration(UserDTO userDTO) throws InvalidMortgageRequest{
		
		
		CheckDate cd = new CheckDate();
		UserRegResponseDTO urr = new UserRegResponseDTO();
		
		User user = new User();

	    Optional<User> us = userrepository.findByphoneNumber(userDTO.getPhoneNumber());
		long age = cd.checkthedata(userDTO.getDateOfBirth(), "y");
		long jobstrtdate = cd.checkthedata(userDTO.getJobStartDate(), "m");
		
		if(!userDTO.getEmploymentStatus().equalsIgnoreCase("employed") || jobstrtdate < 3) {
			throw new InvalidMortgageRequest("Invalid Mortgage Request");
		}
		if(us.isPresent() || userDTO.getPhoneNumber().length() < 10) {
			throw new InvalidMortgageRequest("Please Enter Valid phone number");
		}
		if(age < 18) {
			throw new InvalidMortgageRequest("You should be more than 18 years old to apply for Mortgage");
		}
		
		
		user.setContractTpye(userDTO.getContractTpye());
		user.setFirstName(userDTO.getFirstName());
		user.setOccupation(userDTO.getOccupation());
		user.setSurName(userDTO.getSurName());
		user.setMiddleName(userDTO.getMiddleName());
		user.setTitle(userDTO.getTitle());
		user.setDateOfBirth(userDTO.getDateOfBirth());
		user.setEmail(userDTO.getEmail());
		user.setEmploymentStatus(userDTO.getEmploymentStatus());
		user.setJobStartDate(userDTO.getJobStartDate());
        user.setPhoneNumber(userDTO.getPhoneNumber());
		
		
		String temp = Long.toString(cd.generaterandnumber(2));
		String loginUid = "user" + temp;
		String upassword = cd.generatePassword();
		user.setPassword(upassword);
		user.setLoginUserId(loginUid);
		
		userrepository.save(user);
		usermortgageservice.addUserMortgages(userDTO);
		
		
		List<Long> accnums = accountservice.generateUserAccounts(userDTO);
		
		urr.setMortgageAccountNumber(accnums.get(0));
		urr.setTransactionalAccountNumber(accnums.get(1));
		urr.setLoginUserId(loginUid);
		urr.setPassword(upassword);
		
		
		
		return urr;

		
		
	}



	@Override
	public User userlogin(UserLoginReqDTO userreqDTO) {
		
		User user1 = userrepository.findByLoginUserIdAndPassword(userreqDTO.getLoginUserId(), userreqDTO.getPassword());

		 

        return user1;
	}
	
	

}
